﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Dynamic;

namespace Rey.Api.ExceptionHandle {
    public class ApiExceptionFilter : IExceptionFilter {
        private IHostingEnvironment Env { get; }

        public ApiExceptionFilter(IHostingEnvironment env) {
            Env = env;
        }

        public void OnException(ExceptionContext context) {
            var exception = context.Exception;
            dynamic data = new ExpandoObject();

            if (this.Env.IsDevelopment()) {
                data.error = new DebugApiError(exception);
            } else {
                data.error = new ApiError(exception);
            }

            context.Result = new JsonResult(data);
        }
    }
}
