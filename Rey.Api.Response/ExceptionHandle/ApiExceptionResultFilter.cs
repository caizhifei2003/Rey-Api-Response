﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Dynamic;

namespace Rey.Api.ExceptionHandle {
    public class ApiExceptionResultFilter : IResultFilter {
        public void OnResultExecuted(ResultExecutedContext context) {

        }

        public void OnResultExecuting(ResultExecutingContext context) {
            var result = context.Result;
            if (result is EmptyResult) {
                context.Result = ConvertToJsonResult();
                return;
            }

            var data = (context.Result as ObjectResult)?.Value;
            context.Result = ConvertToJsonResult(data);
        }

        private static IActionResult ConvertToJsonResult(object data = null) {
            dynamic ret = new ExpandoObject();

            if (data != null)
                ret.data = data;
            else
                ret.data = new { };

            var code = ApiExceptionCode.SUCCEEDED;
            var message = ApiException.GetCodeMessage(code);
            ret.error = new { code, message };

            return new JsonResult(ret);
        }
    }
}
