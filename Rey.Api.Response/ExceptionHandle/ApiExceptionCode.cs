﻿namespace Rey.Api.ExceptionHandle {
    public enum ApiExceptionCode : long {
        [CodeMessage("unknown exception")]
        UNKNOWN = -1,
        [CodeMessage("succeeded")]
        SUCCEEDED = 0,

        ACCOUNT_BEGIN = 0x0000_0001,
        [CodeMessage("empty user name")]
        EMPTY_USERNAME,
        [CodeMessage("empty email")]
        EMPTY_EMAIL,
        [CodeMessage("empty password")]
        EMPTY_PASSWORD,
        [CodeMessage("invalid user name")]
        INVALID_USERNAME,
        [CodeMessage("invalid email")]
        INVALID_EMAIL,
        [CodeMessage("invalid password")]
        INVALID_PASSWORD,
        [CodeMessage("exist user name")]
        EXIST_USERNAME,
        [CodeMessage("exist email")]
        EXIST_EMAIL,
        ACCOUNT_END = 0x0000_00FF,

        MODEL_BEGIN = 0x0000_0100,
        [CodeMessage("invalid model id")]
        INVALID_MODEL_ID,
        MODEL_END = 0x0000_01FF,
    }
}
