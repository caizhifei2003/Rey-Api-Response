﻿using System;

namespace Rey.Api.ExceptionHandle {
    [AttributeUsage(AttributeTargets.Field)]
    public class CodeMessageAttribute : Attribute {
        public string Message { get; }
        public CodeMessageAttribute(string message) {
            this.Message = message;
        }
    }
}
