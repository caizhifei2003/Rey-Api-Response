﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Rey.Api.ExceptionHandle;

namespace Rey.Api.Host {
    public class Startup {
        public void ConfigureServices(IServiceCollection services) {
            services.AddMvc(options => {
                options.Filters.Add<ApiExceptionFilter>();
                options.Filters.Add<ApiExceptionResultFilter>();
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            app.Run(async (context) => {
                await context.Response.WriteAsync("Hello World!");
            });
        }
    }
}
